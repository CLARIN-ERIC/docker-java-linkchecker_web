################## 
# GLOBAL ARG
##################
ARG APPLICATION_NAME=linkchecker-web
ARG LOCAL_GIT_DIRECTORY=/tmp/application-git

ARG JAVA_VERSION=17.0.7
ARG JAVA_ALPINE_VERSION=${JAVA_VERSION}_p7-r1
ARG CA_CERTIFICATES_VERSION=20230506-r0

###############
# BUILDER STAGE
###############
FROM registry.gitlab.com/clarin-eric/docker-alpine-base:2.5.0 AS builder

ARG LOCAL_GIT_DIRECTORY
ARG JAVA_ALPINE_VERSION

COPY application-src $LOCAL_GIT_DIRECTORY
WORKDIR $LOCAL_GIT_DIRECTORY

RUN apk --no-cache add \
      "openjdk17=$JAVA_ALPINE_VERSION" \
 && ./mvnw install -DskipTests
 
########################
# IMAGE STAGE
######################## 
FROM registry.gitlab.com/clarin-eric/docker-alpine-supervisor-base:3.5.2

ARG APPLICATION_NAME
ARG LOCAL_GIT_DIRECTORY

ARG JAVA_VERSION
ARG JAVA_ALPINE_VERSION
ARG CA_CERTIFICATES_VERSION

ENV APPLICATION_HOME_DIRECTORY=/app/${APPLICATION_NAME}

RUN mkdir -p /cert ${APPLICATION_HOME_DIRECTORY}/bin ${APPLICATION_HOME_DIRECTORY}/config ${APPLICATION_HOME_DIRECTORY}/logs

COPY --from=builder "${LOCAL_GIT_DIRECTORY}/target/*.jar" "${APPLICATION_HOME_DIRECTORY}/bin/${APPLICATION_NAME}.jar"
COPY init.sh /init/linkchecker-web_init.sh
COPY supervisor/linkchecker-web.conf /etc/supervisor/conf.d/"${APPLICATION_NAME}".conf 

RUN apk --no-cache add \
      "openjdk17-jre=$JAVA_ALPINE_VERSION" \
      "ca-certificates=$CA_CERTIFICATES_VERSION" \
      "openssl=$OPENSSL_VERSION" \
 && chmod u+x '/init/linkchecker-web_init.sh' 
      