#!/bin/bash

#function shutdown()
#{
#    date
#    echo "Shutting down Tomcat"
#    unset CATALINA_PID # Necessary in some cases
#    unset LD_LIBRARY_PATH # Necessary in some cases
#    unset JAVA_OPTS # Necessary in some cases
#
#    ${CATALINA_BASE}/bin/catalina.sh stop
#}

_KEYSTORE_FILE="/cert/server.jks"
_DEFAULT_PASS="password"


if [ -z "${PASSWORD}" ]; then
	echo "Using default STORE_PASS: ${_DEFAULT_PASS}"
	PASSWORD="${_DEFAULT_PASS}"
else
	echo "Using supplied PASSWORD"
fi

#
# Generate SSL certificate if the keystore does not exist
#
if [ -f "${_KEYSTORE_FILE}" ]; then
	echo "Using existing ssl certificate from ${_KEYSTORE_FILE}"
else
	echo "Generating new ssl certificate in ${_KEYSTORE_FILE}"
	keytool -genkey \
		-keyalg RSA \
		-alias selfsigned \
		-keystore "${_KEYSTORE_FILE}" \
 		-storepass "${PASSWORD}" \
		-validity 360 \
		-keysize 2048 \
		-noprompt \
		-dname "cn=clarin.eu, ou=CLARIN, o=CLARIN-ERIC, c=NL" \
		-keypass "${PASSWORD}"
	
	#Exit if keytool command failed
	rc=$?;
	if [[ $rc != 0 ]]; then
		echo "Failed to generate ssl certificate";
		exit $rc;
	fi
fi
